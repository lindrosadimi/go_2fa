package main

import (
	//"byte"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type CodeInfo struct {
	Code      string
	ExpiresAt time.Time
}

var (
	codeMap = make(map[string]CodeInfo)
	mu      sync.Mutex
)

// Générer un code aléatoire à 6 chiffres
func generateCode() string {
	return fmt.Sprintf("%06d", rand.Intn(1000000))
}

// Stocker le code avec l'ID de l'utilisateur et le temps d'expiration
func storeCode(userID string, code string) {
	mu.Lock()
	defer mu.Unlock()
	codeMap[userID] = CodeInfo{
		Code:      code,
		ExpiresAt: time.Now().Add(30 * time.Second), // Temps d'expiration de 30 secondes
	}
}

// Vérifier le code
func verifyCode(userID string, code string) bool {
	mu.Lock()
	defer mu.Unlock()
	// Vérifier si l'utilisateur existe
	info, ok := codeMap[userID]
	if !ok {
		return false
	}

	// Vérifier si le code correspond et n'est pas expiré
	if info.Code == code && time.Now().Before(info.ExpiresAt) {
		delete(codeMap, userID) // Supprimer le code après utilisation
		return true
	}

	return false
}

func generateCodeHandler(w http.ResponseWriter, r *http.Request) {
	userID := r.URL.Query().Get("user_id")

	// Générer un code aléatoire à 6 chiffres
	code := generateCode()

	// Stocker le code avec l'ID de l'utilisateur et le temps d'expiration
	storeCode(userID, code)

	// Envoyer le code à l'utilisateur
	w.Write([]byte(code))
	w.Write([]byte(userID))
}

func verifyCodeHandler(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
    userID := r.FormValue("userID")

	// Vérifier le code
	if verifyCode(userID, code) {
		w.Write([]byte("authorized"))
	} else {
		w.Write([]byte("unauthorized"))
	}
}

func main() {
	http.HandleFunc("/generate-code", generateCodeHandler)
	http.HandleFunc("/verify-code", verifyCodeHandler)

	fmt.Println("Serveur d'authentification démarré sur le port 8080...")
	http.ListenAndServe(":8080", nil)
}
