package main

import (
	"fmt"
	"io"
	"net/http"
)

func main() {
	// Création d'un client HTTP
	client := &http.Client{}

	// Création d'une requête POST vers le serveur d'authentification pour générer le code
	req, err := http.NewRequest("POST", "http://localhost:8080/generate-code?user_id=123", nil)
	if err != nil {
		fmt.Println("Erreur lors de la création de la requête vers le serveur d'authentification :", err)
		return
	}

	// Envoi de la requête au serveur d'authentification
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Erreur lors de l'envoi de la requête au serveur d'authentification :", err)
		return
	}
	defer resp.Body.Close()

	// Lecture du code généré par le serveur d'authentification
	code, err := io.ReadAll(resp.Body)
	// Extraire les 6 premiers caractères pour le token
	token := string(code[:6])

// Extraire le reste pour l'userID
	userID := string(code[6:])

	if err != nil {
		fmt.Println("Erreur lors de la lecture du code généré par le serveur d'authentification :", err)
		return
	}
	//affichage de la réponse du serveur d'authentification
	fmt.Println("L'utilisateur "+string(userID)+" a reçu le code :"+string(token) )


	// Envoi du code au serveur de ressources
	resp, err = http.Get(fmt.Sprintf("http://localhost:8081/protected-resource?code=%s&userID=%s", string(token), string(userID)))
	if err != nil {
		fmt.Println("Erreur lors de l'envoi du code au serveur de ressources :", err)
		return
	}
	defer resp.Body.Close()

	// Lecture de la réponse du serveur de ressources
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Erreur lors de la lecture de la réponse du serveur de ressources :", err)
		return
	}

	// Affichage de la réponse du serveur de ressources 
	fmt.Println("Réponse du serveur de ressources :", string(responseBody))
}
