package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	//"reflect"
)

// Fonction pour vérifier le code auprès du serveur d'authentification
func verifyCodeWithAuthServer(code string, userID string) (string, error) {
	// Créer un payload contenant à la fois le code et le userID
	payload := strings.NewReader(fmt.Sprintf("code=%s&userID=%s", code, userID))
	
	// Effectuer une requête POST avec le payload contenant le code et le userID
	resp, err := http.Post("http://localhost:8080/verify-code", "application/x-www-form-urlencoded", payload)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Lire la réponse du serveur d'authentification
	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	// Retourner le statut de l'authentification
	return string(responseBody), nil
}

// Fonction pour gérer la demande de la ressource protégée
func protectedResourceHandler(w http.ResponseWriter, r *http.Request) {
	// Récupérer le code envoyé par le client
	code := r.URL.Query().Get("code")
	userID := r.URL.Query().Get("userID")

	// Vérifier le code auprès du serveur d'authentification
	authStatus, err := verifyCodeWithAuthServer(code, userID)
	
	if err != nil {
		http.Error(w, "Erreur lors de la vérification du code avec le serveur d'authentification ", http.StatusInternalServerError)
		return
	}
	
	// Traiter le statut de l'authentification
	switch authStatus {
	case "authorized":
		// Si le code est correct, accorder l'accès à la ressource
		fmt.Fprintln(w, "Accès autorisé à la ressource")
	case "unauthorized":
		// Si le code est incorrect ou expiré, refuser l'accès à la ressource
		http.Error(w, "Accès refusé à la ressource", http.StatusUnauthorized)
	default:
		// Autre traitement des erreurs d'authentification
		http.Error(w, "Erreur lors de la vérification du code avec le serveur d'authentification dans le switch", http.StatusInternalServerError)
	}
}

func main() {
	// Mise en place d'un gestionnaire pour la route de la ressource protégée
	http.HandleFunc("/protected-resource", protectedResourceHandler)

	// Démarrage du serveur sur le port 8081
	fmt.Println("Serveur de ressources démarré sur le port 8081...")
	http.ListenAndServe(":8081", nil)
}
