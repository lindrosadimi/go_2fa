### Comment lancer le projet
Pour lancer le projet il faut d'abord aller dans les répertoires suivants :
    - authServer
    - ressourceServer
Et lancer la commande :
    - go run main.go

Ensuite se rendre à l'adresse : http://localhost:8080/generate-code?user_id=123 pour générer le token.

NOTE IMPORTANTE : Les 6 premiers chiffres correspondent à votre token, la suite est votre numéro d'utilisateur.

Après cela se rendre à l'adresse : http://localhost:8081/protected-resource?code=098482&userID=123 pour vérifier si l'accès est autorisé ou non.

Prenez le soin de modifier le code d'authentification avec celui générer par la page précédente (6 premiers chiffres)
